﻿namespace Protocol.Data;

public class OverridePackage
{
	public int I { get; set; }

	[JsonIgnore]
	public int Id
	{
		get => I;
		set => I = value;
	}

	public long C { get; set; }

	[JsonIgnore]
	public long CompanyId
	{
		get => C;
		set => C = value;
	}

	public string Co { get; set; } = "";

	[JsonIgnore]
	public string CompanyCode
	{
		get => Co;
		set => Co = value;
	}

	public short S { get; set; }

	[JsonIgnore]
	public short SortIndex
	{
		get => S;
		set => S = value;
	}

	public int F { get; set; }

	[JsonIgnore]
	public int FromPackageTypeId
	{
		get => F;
		set => F = value;
	}

	public int T { get; set; }

	[JsonIgnore]
	public int ToPackageTypeId
	{
		get => T;
		set => T = value;
	}
}

public class OverridePackageList : List<OverridePackage>;

public class RedirectPackage
{
	public int I { get; set; }

	[JsonIgnore]
	public int Id
	{
		get => I;
		set => I = value;
	}

	public long A { get; set; }

	[JsonIgnore]
	public long CompanyId
	{
		get => A;
		set => A = value;
	}

	public string Co { get; set; } = "";

	[JsonIgnore]
	public string CompanyCode
	{
		get => Co;
		set => Co = value;
	}

	public int O { get; set; }

	[JsonIgnore]
	public int OverrideId
	{
		get => O;
		set => O = value;
	}

	public int F { get; set; }

	[JsonIgnore]
	public int FromZoneId
	{
		get => F;
		set => F = value;
	}

	public int T { get; set; }

	[JsonIgnore]
	public int ToZoneId
	{
		get => T;
		set => T = value;
	}

	public int S { get; set; }

	[JsonIgnore]
	public int ServiceLevelId
	{
		get => S;
		set => S = value;
	}

	public int P { get; set; }

	[JsonIgnore]
	public int PackageTypeId
	{
		get => P;
		set => P = value;
	}

	public decimal C { get; set; }

	[JsonIgnore]
	public decimal Charge
	{
		get => C;
		set => C = value;
	}
}

public class RedirectPackageList : List<RedirectPackage>;