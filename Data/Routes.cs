﻿namespace Protocol.Data;

public class RouteBase : ProgramData
{
	public string CustomerCode { get; set; } = "";
	public string RouteName    { get; set; } = "";


	public RouteBase( string programName ) : base( programName )
	{
	}

	[Flags]
	public enum TRAVEL_MODE : ushort
	{
		SHORTEST = 0, // Bit 0 off
		FASTEST  = 1,

		AVOID_TOLL_ROADS       = 1 << 1,
		AVOID_MOTORWAYS        = 1 << 2,
		AVOID_FERRIES          = 1 << 3,
		AVOID_BORDER_CROSSINGS = 1 << 4,
		AVOID_CAR_POOLS        = 1 << 5,
		AVOID_UNPAVED_ROADS    = 1 << 6,

		PICKUP_AND_DELIVERIES = 1 << 7,
		DONT_OPTIMISE         = 1 << 8
	}
}

public class Route : RouteBase
{
	public bool Monday    { get; set; }
	public bool Tuesday   { get; set; }
	public bool Wednesday { get; set; }
	public bool Thursday  { get; set; }
	public bool Friday    { get; set; }
	public bool Saturday  { get; set; }
	public bool Sunday    { get; set; }

	public TimeSpan StartTime { get; set; } = new();
	public TimeSpan EndTime   { get; set; } = new();

	public bool    IsStatic    { get; set; }
	public short   StaticCount { get; set; }
	public decimal Commission  { get; set; }

	public string BillingCompanyCode { get; set; } = "";

	public bool ShowOnMobileApp { get; set; }

	public Route( string programName ) : base( programName )
	{
	}
}

public class RouteAndCompanyAddresses : Route
{
	public Companies CompaniesInRoute { get; set; } = [];

	public RouteAndCompanyAddresses( string programName ) : base( programName )
	{
	}
}

public class RouteCompany : RouteBase
{
	public string RouteCompanyCode { get; set; } = "";
	public bool   Delete           { get; set; }

	public RouteCompany( string programName ) : base( programName )
	{
	}
}

public class RouteLookup : PreFetch
{
	public string CustomerCode { get; set; } = "";
}

public class RouteOptions
{
	public bool Option1 { get; set; }
	public bool Option2 { get; set; }
	public bool Option3 { get; set; }
	public bool Option4 { get; set; }
	public bool Option5 { get; set; }
	public bool Option6 { get; set; }
	public bool Option7 { get; set; }
	public bool Option8 { get; set; }

	public RouteOptions()
	{
	}

	public RouteOptions( RouteOptions r )
	{
		Option1 = r.Option1;
		Option2 = r.Option2;
		Option3 = r.Option3;
		Option4 = r.Option4;
		Option5 = r.Option5;
		Option6 = r.Option6;
		Option7 = r.Option7;
		Option8 = r.Option8;
	}
}

public class RouteUpdate : RouteBase
{
	public string CompanyName { get; set; } = "";

	public RouteOptions Options { get; set; } = new();

	public RouteUpdate( string programName ) : base( programName )
	{
	}
}

public class RouteCompanySummary : CompanyByAccountSummary
{
	public RouteOptions Options { get; set; } = new();

	public RouteCompanySummary()
	{
	}

	public RouteCompanySummary( RouteCompanySummary r ) : base( r )
	{
		Options = new RouteOptions( r.Options );
	}

	public RouteCompanySummary( CompanyByAccountSummary r ) : base( r )
	{
	}
}

public class RouteCompanySummaryList : List<RouteCompanySummary>;

public class CompanyAndOptions : RouteOptions
{
	public string CompanyName { get; set; } = "";
}

public class RouteCompanyList : ProgramData
{
	public string                  CustomerCode   { get; set; } = "";
	public string                  RouteName      { get; set; } = "";
	public List<CompanyAndOptions> CompanyOptions { get; set; } = [];

	public RouteCompanyList( string programName ) : base( programName )
	{
	}
}

public class RouteSchedule
{
	public string RouteName { get; set; } = "";

	public bool Monday    { get; set; }
	public bool Tuesday   { get; set; }
	public bool Wednesday { get; set; }
	public bool Thursday  { get; set; }
	public bool Friday    { get; set; }
	public bool Saturday  { get; set; }
	public bool Sunday    { get; set; }

	public TimeSpan StartTime { get; set; }
	public TimeSpan EndTime   { get; set; }

	public bool    IsStatic    { get; set; }
	public short   StaticCount { get; set; }
	public decimal Commission  { get; set; }

	public string BillingCompanyCode { get; set; } = "";
	public bool   ShowOnMobileApp    { get; set; }
}

public class RouteSummary : RouteOptions
{
	public string CompanyName  { get; set; } = "";
	public string Suite        { get; set; } = "";
	public string AddressLine1 { get; set; } = "";
}

public class RouteScheduleSummary : RouteSchedule
{
	public string CompanyName  { get; set; } = "";
	public string Suite        { get; set; } = "";
	public string AddressLine1 { get; set; } = "";
}

public class RouteScheduleAndRoutes : RouteSchedule
{
	public List<RouteSummary> RouteSummary = [];
}

public class CustomerRoutesSchedule
{
	public string                       CustomerCode          { get; set; } = "";
	public bool                         EnableRoutesForDevice { get; set; }
	public List<RouteScheduleAndRoutes> ScheduleAndRoutes = [];
}

public class CustomerRoutesScheduleList : List<CustomerRoutesSchedule>;

public class RouteLookupSummaryList : List<RouteSchedule>;

public class RouteScheduleUpdate : RouteSchedule
{
	public string ProgramName  { get; set; } = "";
	public string CustomerCode { get; set; } = "";
}

public class EnableRouteForeCustomer
{
	public string CustomerCode { get; set; } = "";
	public bool   Enable       { get; set; }
}

public class CustomerRouteBasic
{
	public string N { get; set; } = "";

	[JsonIgnore]
	public string RouteName
	{
		get => N;
		set => N = value;
	}


	public bool E { get; set; }

	[JsonIgnore]
	public bool Enabled
	{
		get => E;
		set => E = value;
	}
}

public class CustomerRoutesBasic
{
	public List<CustomerRouteBasic> R { get; set; } = [];

	[JsonIgnore]
	public List<CustomerRouteBasic> Routes
	{
		get => R;
		set => R = value;
	}

	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}


	public bool E { get; set; }

	[JsonIgnore]
	public bool EnableRoutesForCustomer
	{
		get => E;
		set => E = value;
	}
}

public class CustomersRoutesBasic : List<CustomerRoutesBasic>;

public class AddUpdateRenameRouteBasic
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}


	public string O { get; set; } = "";

	[JsonIgnore]
	public string OldName
	{
		get => O;
		set => O = value;
	}


	public string N { get; set; } = "";

	[JsonIgnore]
	public string NewName
	{
		get => N;
		set => N = value;
	}


	public bool E { get; set; } = true;

	[JsonIgnore]
	public bool Enabled
	{
		get => E;
		set => E = value;
	}
}