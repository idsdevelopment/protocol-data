﻿namespace Protocol.Data;

public class Messaging
{
	public const string DRIVERS_BOARD  = "DRIVERS_BOARD",
	                    DISPATCH_BOARD = "DISPATCH_BOARD",
	                    DRIVERS        = "DRIVERS",
	                    SEARCH_TRIPS   = "SEARCH_TRIPS";
}

public class TripUpdateStatusBase
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string Program
	{
		get => P;
		set => P = value;
	}

	public DEVICE_STATUS Ds { get; set; } = DEVICE_STATUS.UNSET;

	[JsonIgnore]
	public DEVICE_STATUS DeviceStatus
	{
		get => Ds;
		set => Ds = value;
	}

	[JsonIgnore]
	public bool ReceivedByDevice
	{
		get => ( Ds & DEVICE_STATUS.RECEIVED_BY_DEVICE ) != 0;
		set => Ds = value ? Ds | DEVICE_STATUS.RECEIVED_BY_DEVICE : Ds & ~DEVICE_STATUS.RECEIVED_BY_DEVICE;
	}


	[JsonIgnore]
	public bool ReadByDriver
	{
		get => ( Ds & DEVICE_STATUS.READ_BY_DRIVER ) != 0;
		set => Ds = value ? Ds | DEVICE_STATUS.READ_BY_DRIVER : Ds & ~DEVICE_STATUS.READ_BY_DRIVER;
	}

	public STATUS S { get; set; } = STATUS.UNSET;

	[JsonIgnore]
	public STATUS Status
	{
		get => S;
		set => S = value;
	}

	public STATUS1 S1 { get; set; } = STATUS1.UNSET;

	[JsonIgnore]
	public STATUS1 Status1
	{
		get => S1;
		set => S1 = value;
	}


	public STATUS2 S2 { get; set; } = STATUS2.UNSET;

	[JsonIgnore]
	public STATUS2 Status2
	{
		get => S2;
		set => S2 = value;
	}


	public List<string> Ti { get; set; } = [];

	[JsonIgnore]
	public List<string> TripIdList
	{
		get => Ti;
		set => Ti = value;
	}


	public string D { get; set; } = "";

	[JsonIgnore]
	public string Driver
	{
		get => D;
		set => D = value;
	}

	public TripUpdateStatusBase()
	{
	}

	public TripUpdateStatusBase( TripUpdateStatusBase b )
	{
		Program      = b.Program;
		DeviceStatus = b.DeviceStatus;
		Status       = b.Status;
		Status1      = b.Status1;
		Status2      = b.Status2;
		TripIdList   = b.TripIdList;
		Driver       = b.Driver;
	}

	[Flags]
	public enum DEVICE_STATUS : ushort
	{
		UNSET,
		RECEIVED_BY_DEVICE = 1,
		READ_BY_DRIVER     = 1 << 1
	}
}

public class TripUpdateStatus : TripUpdateStatusBase
{
	public BROADCAST B { get; set; } = BROADCAST.NONE;

	[JsonIgnore]
	public BROADCAST Broadcast
	{
		get => B;
		set => B = value;
	}

	[JsonIgnore]
	public bool BroadcastToDriversBoard
	{
		get => ( B & BROADCAST.DRIVERS_BOARD ) != 0;
		set => B = value ? B | BROADCAST.DRIVERS_BOARD : B & ~BROADCAST.DRIVERS_BOARD;
	}


	[JsonIgnore]
	public bool BroadcastToDispatchBoard
	{
		get => ( B & BROADCAST.DISPATCH_BOARD ) != 0;
		set => B = value ? B | BROADCAST.DISPATCH_BOARD : B & ~BROADCAST.DISPATCH_BOARD;
	}

	[JsonIgnore]
	public bool BroadcastToDriver
	{
		get => ( B & BROADCAST.DRIVER ) != 0;
		set => B = value ? B | BROADCAST.DRIVER : B & ~BROADCAST.DRIVER;
	}

	[JsonIgnore]
	public bool BroadcastToSearchTrips
	{
		get => ( B & BROADCAST.SEARCH_TRIPS ) != 0;
		set => B = value ? B | BROADCAST.SEARCH_TRIPS : B & ~BROADCAST.SEARCH_TRIPS;
	}


	public TripUpdateStatus()
	{
	}

	public TripUpdateStatus( TripUpdateStatus s ) : base( s )
	{
		Broadcast = s.Broadcast;
	}

	[Flags]
	public enum BROADCAST : ushort
	{
		NONE,
		DRIVERS_BOARD  = 1,
		DISPATCH_BOARD = 1 << 1,
		DRIVER         = 1 << 2,
		SEARCH_TRIPS   = 1 << 3,
		CLIENTS        = DRIVERS_BOARD | DISPATCH_BOARD | SEARCH_TRIPS,
		ALL            = CLIENTS | DRIVER
	}
}

public class TripUpdateMessage : TripUpdateStatus
{
	public ACTION A { get; set; } = ACTION.UPDATE_STATUS;

	[JsonIgnore]
	public ACTION Action
	{
		get => A;
		set => A = value;
	}


	public string Bo { get; set; } = "";

	[JsonIgnore]
	public string Board
	{
		get => Bo;
		set => Bo = value;
	}


	public List<string> Ts { get; set; } = [];

	[JsonIgnore]
	public List<string> TripSortOrder
	{
		get => Ts;
		set => Ts = value;
	}


	public TripUpdateMessage()
	{
	}

	public TripUpdateMessage( TripUpdateMessage t ) : base( t )
	{
		Action = t.Action;
		Board  = t.Board;
	}

	public TripUpdateMessage( ACTION action, string board, TripUpdateStatus s ) : base( s )
	{
		Action = action;
		Board  = board;
	}

	public enum ACTION
	{
		UPDATE_STATUS,
		UPDATE_TRIP
	}
}