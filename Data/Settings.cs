﻿namespace Protocol.Data;

public class Settings
{
	public long LastAccountUpdate { get; set; }
	public long LastTripUpdate    { get; set; }
	public long LastAddressUpdate { get; set; }
}