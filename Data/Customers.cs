﻿namespace Protocol.Data;

public class AddUpdateCustomer : ProgramData
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}

	public string S { get; set; } = "";

	[JsonIgnore]
	public string SuggestedLoginCode
	{
		get => S;
		set => S = value;
	}

	public string U { get; set; } = "";

	[JsonIgnore]
	public string UserName
	{
		get => U;
		set => U = value;
	}

	public string P { get; set; } = "";

	[JsonIgnore]
	public string Password
	{
		get => P;
		set => P = value;
	}

	public bool L { get; set; }

	[JsonIgnore]
	public bool LoginEnabled
	{
		get => L;
		set => L = value;
	}

	public Company Co { get; set; } = new();

	[JsonIgnore]
	public Company Company
	{
		get => Co;
		set => Co = value;
	}

	public Company Bc { get; set; } = new();

	[JsonIgnore]
	public Company BillingCompany
	{
		get => Bc;
		set => Bc = value;
	}

	public Companies Cs { get; set; } = [];

	[JsonIgnore]
	public Companies Companies
	{
		get => Cs;
		set => Cs = value;
	}

	public AddUpdateCustomer( string programName ) : base( programName )
	{
	}
}

public class CustomerCodeList : List<string>
{
	public CustomerCodeList()
	{
	}

	public CustomerCodeList( IEnumerable<string> cList )
	{
		AddRange( cList );
	}
}

public class CustomerCodeCompanyName
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}

	public string N { get; set; } = "";

	[JsonIgnore]
	public string CompanyName
	{
		get => N;
		set => N = value;
	}
}

public class CustomerCodeCompanyNameList : List<CustomerCodeCompanyName>;

public class AddCustomerCompany : ProgramData
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}

	public Company Co { get; set; } = new();

	[JsonIgnore]
	public Company Company
	{
		get => Co;
		set => Co = value;
	}

	public AddCustomerCompany( string programName ) : base( programName )
	{
	}
}

public class DeleteCustomerCompany : ProgramData
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}

	public Company Co { get; set; } = new();

	[JsonIgnore]
	public Company Company
	{
		get => Co;
		set => Co = value;
	}

	public DeleteCustomerCompany( string programName ) : base( programName )
	{
	}
}

public class UpdateCustomerCompany : ProgramData
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => C;
		set => C = value;
	}

	public Company Co { get; set; } = new();

	[JsonIgnore]
	public Company Company
	{
		get => Co;
		set => Co = value;
	}

	public bool G { get; set; }

	[JsonIgnore]
	public bool GlobalUpdate
	{
		get => G;
		set => G = value;
	}

	public UpdateCustomerCompany( string programName ) : base( programName )
	{
	}
}

public class CustomerLookup : PreFetch
{
	public bool ByAccountNumber { get; set; }
}

public class CustomerLookupSummary : CompanyByAccountSummary
{
	public bool BoolOption1 { get; set; }
}

public class CustomerLookupSummaryList : List<CustomerLookupSummary>
{
	public CustomerLookupSummaryList()
	{
	}

	public CustomerLookupSummaryList( IEnumerable<CustomerLookupSummary> list )
	{
		AddRange( list );
	}
}

public class CustomerCompanies
{
	public Companies C { get; set; } = [];

	[JsonIgnore]
	public Companies Companies
	{
		get => C;
		set => C = value;
	}

	public string Cu { get; set; } = "";

	[JsonIgnore]
	public string CustomerCode
	{
		get => Cu;
		set => Cu = value;
	}
}

public class CustomerCompaniesList : List<CustomerCompanies>;