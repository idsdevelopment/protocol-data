﻿namespace Protocol.Data.Customers.Priority;

public class UpdatePickupAddress : Company
{
	public string T { get; set; } = "";

	[JsonIgnore]
	public string TripId
	{
		get => T;
		set => T = value;
	}

	public string P0 { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P0;
		set => P0 = value;
	}
}