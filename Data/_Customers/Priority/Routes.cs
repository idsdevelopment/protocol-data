﻿namespace Protocol.Data.Customers.Priority;

public class PriorityPharmacyRoutes
{
	public string Pharmacy     { get; set; } = "";
	public string LocationCode { get; set; } = "";

	public List<string> RouteNames { get; set; } = [];
}

public class PriorityDeviceRoutes : List<PriorityPharmacyRoutes>;