﻿namespace Protocol.Data.Customers.Priority;

public class PriorityDriverCommissionRange
{
	public string D { get; set; } = "";

	[JsonIgnore]
	public string Driver
	{
		get => D;
		set => D = value;
	}


	public DateTimeOffset F { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset FromDate
	{
		get => F;
		set => F = value;
	}


	public DateTimeOffset T { get; set; } = DateTimeOffset.MaxValue;

	[JsonIgnore]
	public DateTimeOffset ToDate
	{
		get => T;
		set => T = value;
	}
}

public class PriorityDriverCommissions : List<PriorityDriverCommission>;

public class PriorityDriverCommission
{
	public Address De { get; set; } = new();

	[JsonIgnore]
	public Address DeliveryAddress
	{
		get => De;
		set => De = value;
	}


	public DateTimeOffset Da { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTime
	{
		get => Da;
		set => Da = value;
	}

	public string T { get; set; } = "";

	[JsonIgnore]
	public string TripId
	{
		get => T;
		set => T = value;
	}


	public string Re { get; set; } = "";

	[JsonIgnore]
	public string Reference
	{
		get => Re;
		set => Re = value;
	}


	public decimal Di { get; set; }

	[JsonIgnore]
	public decimal Distance
	{
		get => Di;
		set => Di = value;
	}


	public decimal C { get; set; }

	[JsonIgnore]
	public decimal Commission
	{
		get => C;
		set => C = value;
	}


	public string Sl { get; set; } = "";

	[JsonIgnore]
	public string ServiceLevel
	{
		get => Sl;
		set => Sl = value;
	}


	public string M { get; set; } = "";

	[JsonIgnore]
	public string ManifestName
	{
		get => M;
		set => M = value;
	}

	public int L { get; set; }

	[JsonIgnore]
	public int LegNumber
	{
		get => L;
		set => L = value;
	}


	public string E { get; set; } = "";

	[JsonIgnore]
	public string Error
	{
		get => E;
		set => E = value;
	}

	public class Address
	{
		public string C { get; set; } = "";

		[JsonIgnore]
		public string CompanyName
		{
			get => C;
			set => C = value;
		}


		public string S { get; set; } = "";

		[JsonIgnore]
		public string Street
		{
			get => S;
			set => S = value;
		}


		public string I { get; set; } = "";

		[JsonIgnore]
		public string City
		{
			get => I;
			set => I = value;
		}
	}
}

public class UpdateDriverCommission
{
	public string A { get; set; } = "";

	[JsonIgnore]
	public string AccountId
	{
		get => A;
		set => A = value;
	}


	public string S { get; set; } = "";

	[JsonIgnore]
	public string StaffId
	{
		get => S;
		set => S = value;
	}


	public string Re { get; set; } = "";

	[JsonIgnore]
	public string Reference
	{
		get => Re;
		set => Re = value;
	}


	public int O { get; set; }

	[JsonIgnore]
	public int SortOrder
	{
		get => O;
		set => O = value;
	}


	public decimal D { get; set; }

	[JsonIgnore]
	public decimal Distance
	{
		get => D;
		set => D = value;
	}


	public DateTimeOffset Da { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTime
	{
		get => Da;
		set => Da = value;
	}


	public string Sl { get; set; } = "";

	[JsonIgnore]
	public string ServiceLevel
	{
		get => Sl;
		set => Sl = value;
	}


	public string T { get; set; } = "";

	[JsonIgnore]
	public string TripId
	{
		get => T;
		set => T = value;
	}


	public Maps.Route.Route R { get; set; } = [];

	[JsonIgnore]
	public Maps.Route.Route Route
	{
		get => R;
		set => R = value;
	}


	public string Pc { get; set; } = "";

	[JsonIgnore]
	public string PickupCompany
	{
		get => Pc;
		set => Pc = value;
	}


	public string Ps { get; set; } = "";

	[JsonIgnore]
	public string PickupStreet
	{
		get => Ps;
		set => Ps = value;
	}


	public string Pi { get; set; } = "";

	[JsonIgnore]
	public string PickupCity
	{
		get => Pi;
		set => Pi = value;
	}


	public string Dc { get; set; } = "";

	[JsonIgnore]
	public string DeliveryCompany
	{
		get => Dc;
		set => Dc = value;
	}


	public string Ds { get; set; } = "";

	[JsonIgnore]
	public string DeliveryStreet
	{
		get => Ds;
		set => Ds = value;
	}


	public string Di { get; set; } = "";

	[JsonIgnore]
	public string DeliveryCity
	{
		get => Di;
		set => Di = value;
	}


	public string F { get; set; } = "";

	[JsonIgnore]
	public string Formula
	{
		get => F;
		set => F = value;
	}
}