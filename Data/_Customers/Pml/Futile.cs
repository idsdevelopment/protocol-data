﻿namespace Protocol.Data._Customers.Pml;

public class Futile
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P;
		set => P = value;
	}


	public List<string> T { get; set; } = [];

	[JsonIgnore]
	public List<string> TripIds
	{
		get => T;
		set => T = value;
	}


	public STATUS S { get; set; } = STATUS.NEW;

	[JsonIgnore]
	public STATUS Status
	{
		get => S;
		set => S = value;
	}


	public string R { get; set; } = "";

	[JsonIgnore]
	public string Reason
	{
		get => R;
		set => R = value;
	}


	public DateTimeOffset D { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTime
	{
		get => D;
		set => D = value;
	}
}