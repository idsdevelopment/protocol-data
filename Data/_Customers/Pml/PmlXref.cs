﻿namespace Protocol.Data._Customers.Pml;

public class PmlXref
{
	public string L { get; set; } = "";

	[JsonIgnore]
	public string Location
	{
		get => L;
		set => L = value;
	}


	public string A { get; set; } = "";

	[JsonIgnore]
	public string AccountNumber
	{
		get => A;
		set => A = value;
	}


	public string G { get; set; } = "";

	[JsonIgnore]
	public string GlnNumber
	{
		get => G;
		set => G = value;
	}
}