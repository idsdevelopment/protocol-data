﻿namespace Protocol.Data;

public class GetInvoices
{
	public DateTimeOffset F { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset FromDateTime
	{
		get => F;
		set => F = value;
	}

	public DateTimeOffset T { get; set; } = DateTimeOffset.MaxValue;

	[JsonIgnore]
	public DateTimeOffset ToDateTime
	{
		get => T;
		set => T = value;
	}

	/// <summary>
	///     Blank, All Billing Customers
	/// </summary>
	public string B { get; set; } = "";

	[JsonIgnore]
	public string BillingCustomerCode
	{
		get => B;
		set => B = value;
	}
}

public class Invoice
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string ProgramName
	{
		get => P;
		set => P = value;
	}

	public bool C { get; set; }

	[JsonIgnore]
	public bool IsCreditNode
	{
		get => C;
		set => C = value;
	}

	public string B { get; set; } = "";

	[JsonIgnore]
	public string BillingCompanyName
	{
		get => B;
		set => B = value;
	}

	public long I { get; set; } = -1;

	[JsonIgnore]
	public long InvoiceNumber => I;

	public DateTimeOffset D { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset InvoiceDateTime
	{
		get => D;
		set => D = value;
	}

	public decimal Tv { get; set; }

	[JsonIgnore]
	public decimal TaxableValue
	{
		get => Tv;
		set => Tv = value;
	}

	public decimal Nv { get; set; }

	[JsonIgnore]
	public decimal NonTaxableValue
	{
		get => Nv;
		set => Nv = value;
	}

	public decimal Ta { get; set; }

	[JsonIgnore]
	public decimal TotalTaxA
	{
		get => Ta;
		set => Ta = value;
	}

	public decimal Tb { get; set; }

	[JsonIgnore]
	public decimal TotalTaxB
	{
		get => Tb;
		set => Tb = value;
	}

	public decimal T { get; set; }

	[JsonIgnore]
	public decimal TotalValue
	{
		get => T;
		set => T = value;
	}

	public decimal Tc { get; set; }

	[JsonIgnore]
	public decimal TotalCharges
	{
		get => Tc;
		set => Tc = value;
	}

	public List<string> Ti { get; set; } = [];

	[JsonIgnore]
	public List<string> TripIds
	{
		get => Ti;
		set => Ti = value;
	}

	public List<TripCharge> Cc { get; set; } = [];

	[JsonIgnore]
	public List<TripCharge> CombinedTripCharges
	{
		get => Cc;
		set => Cc = value;
	}

	public DateTimeOffset If { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset InvoiceFrom
	{
		get => If;
		set => If = value;
	}

	public DateTimeOffset It { get; set; } = DateTimeOffset.MaxValue;

	[JsonIgnore]
	public DateTimeOffset InvoiceTo
	{
		get => It;
		set => It = value;
	}

	public decimal Di { get; set; }

	[JsonIgnore]
	public decimal TotalDiscount
	{
		get => Di;
		set => Di = value;
	}

	public bool E { get; set; }

	[JsonIgnore]
	public bool EmailInvoice
	{
		get => E;
		set => E = value;
	}

	public short Bp { get; set; }

	[JsonIgnore]
	public short BillingPeriod
	{
		get => Bp;
		set => Bp = value;
	}

	public decimal R { get; set; }

	[JsonIgnore]
	public decimal Residual
	{
		get => R;
		set => R = value;
	}

	public string Ea { get; set; } = "";

	[JsonIgnore]
	public string EmailAddress
	{
		get => Ea;
		set => Ea = value;
	}

	public string Es { get; set; } = "";

	[JsonIgnore]
	public string EmailSubject
	{
		get => Es;
		set => Es = value;
	}

	public Invoice()
	{
	}

	public Invoice( Invoice i )
	{
		BillingCompanyName  = i.BillingCompanyName;
		BillingPeriod       = i.BillingPeriod;
		CombinedTripCharges = i.CombinedTripCharges;
		InvoiceDateTime     = i.InvoiceDateTime;
		InvoiceFrom         = i.InvoiceFrom;
		I                   = i.InvoiceNumber;
		InvoiceTo           = i.InvoiceTo;
		IsCreditNode        = i.IsCreditNode;
		NonTaxableValue     = i.NonTaxableValue;
		ProgramName         = i.ProgramName;
		Residual            = i.Residual;
		TaxableValue        = i.TaxableValue;
		TotalCharges        = i.TotalCharges;
		TotalDiscount       = i.TotalDiscount;
		TotalTaxA           = i.TotalTaxA;
		TotalTaxB           = i.TotalTaxB;
		TotalValue          = i.TotalValue;
		TripIds             = i.TripIds;

		EmailInvoice = i.EmailInvoice;
		EmailAddress = i.EmailAddress;
		EmailSubject = i.EmailSubject;
	}
}

public class Invoices : List<Invoice>;

public class InvoiceAndTrips : Invoice
{
	public List<Trip> Tl { get; set; } = [];

	[JsonIgnore]
	public List<Trip> Trips
	{
		get => Tl;
		set => Tl = value;
	}

	public InvoiceAndTrips()
	{
	}

	public InvoiceAndTrips( Invoice i, List<Trip> trips ) : base( i )
	{
		Trips = trips;
	}
}

public class InvoiceAndTripsList : List<InvoiceAndTrips>;