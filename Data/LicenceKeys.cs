﻿namespace Protocol.Data;

public class LicenseKeys
{
	public string D { get; set; } = "";

	[JsonIgnore]
	public string Dynamsoft
	{
		get => D;
		set => D = value;
	}
}