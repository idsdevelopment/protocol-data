﻿namespace Protocol.Data;

// ReSharper disable once InconsistentNaming
public enum LOG
{
	LOGIN,
	STAFF,
	TRIP
}

public class LogLookup
{
	public LOG            Log      { get; set; }
	public string         Key      { get; set; } = "";
	public DateTimeOffset FromDate { get; set; } = DateTimeOffset.MinValue;
	public DateTimeOffset ToDate   { get; set; } = DateTimeOffset.MaxValue;
}

public class LogEntry
{
	public string P { get; set; } = "";

	[JsonIgnore]
	public string PartitionKey
	{
		get => P;
		set => P = value;
	}

	public string R { get; set; } = "";

	[JsonIgnore]
	public string RowKey
	{
		get => R;
		set => R = value;
	}

	public DateTimeOffset T { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset Timestamp
	{
		get => T;
		set => T = value;
	}

	public string O { get; set; } = "";

	[JsonIgnore]
	public string Operation
	{
		get => O;
		set => O = value;
	}

	public string D { get; set; } = "";

	[JsonIgnore]
	public string Description
	{
		get => D;
		set => D = value;
	}

	public string Pg { get; set; } = "";

	[JsonIgnore]
	public string Program
	{
		get => Pg;
		set => Pg = value;
	}

	public string Da { get; set; } = "";

	[JsonIgnore]
	public string Data
	{
		get => Da;
		set => Da = value;
	}

	public int To { get; set; }

	[JsonIgnore]
	public int TimeZoneOffset
	{
		get => To;
		set => To = value;
	}

	public LogEntry()
	{
	}

	public LogEntry( LogEntry e )
	{
		P  = e.P;
		R  = e.R;
		T  = e.T;
		O  = e.O;
		D  = e.D;
		Pg = e.Pg;
		Da = e.Da;
		To = e.To;
	}
}

public class Log : List<LogEntry>;