﻿namespace Protocol.Data;

public static class Time
{
	public static readonly TimeSpan MinDayTimeSpan = new( 0, 0, 0, 0, 0 );
	public static readonly TimeSpan MaxDayTimeSpan = new( 0, 23, 59, 59, 999 );
}