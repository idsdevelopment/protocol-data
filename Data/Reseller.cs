﻿namespace Protocol.Data;

public class ResellersCustomerCompany : CompanyBase
{
	public string CustomerCode { get; set; } = "";

	public ResellersCustomerCompany()
	{
	}

	public ResellersCustomerCompany( Address address ) : base( address )
	{
	}
}

public class ResellersCustomerCompanyLookup : ResellersCustomerCompany
{
	public bool Ok { get; set; }

	public ResellersCustomerCompanyLookup()
	{
	}

	public ResellersCustomerCompanyLookup( Address address ) : base( address )
	{
	}
}