﻿namespace Protocol.Data;

public class Role
{
	public string N { get; set; } = "";

	[JsonIgnore]
	public string Name
	{
		get => N;
		set => N = value;
	}


	public bool M { get; set; }

	[JsonIgnore]
	public bool Mandatory
	{
		get => M;
		set => M = value;
	}

	public string J { get; set; } = "";

	[JsonIgnore]
	public string JsonObject
	{
		get => J;
		set => J = value;
	}


	public bool A { get; set; }

	[JsonIgnore]
	public bool IsAdministrator
	{
		get => A;
		set => A = value;
	}


	public bool D { get; set; }

	[JsonIgnore]
	public bool IsDriver
	{
		get => D;
		set => D = value;
	}


	public bool W { get; set; }

	[JsonIgnore]
	public bool IsWebService
	{
		get => W;
		set => W = value;
	}
}

public class RoleRename : Role
{
	public string Nn { get; set; } = "";

	[JsonIgnore]
	public string NewName
	{
		get => Nn;
		set => Nn = value;
	}
}

public class Roles : List<Role>
{
	public Roles()
	{
	}

	public Roles( IEnumerable<Role> roles ) : base( roles )
	{
	}
}

public class DeleteRoles : List<string>;