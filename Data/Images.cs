﻿namespace Protocol.Data;

public sealed class Image
{
	public DateTimeOffset T { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTime
	{
		get => T;
		set => T = value;
	}


	public string N { get; set; } = "";

	[JsonIgnore]
	public string Name
	{
		get => N;
		set => N = value;
	}


	public STATUS S { get; set; } = STATUS.UNKNOWN;

	[JsonIgnore]
	public STATUS Status
	{
		get => S;
		set => S = value;
	}


	public byte[] B { get; set; } = Array.Empty<byte>();

	[JsonIgnore]
	public byte[] Bytes
	{
		get => B;
		set => B = value;
	}


	public Dictionary<string, string> M { get; set; } = new();

	[JsonIgnore]
	public Dictionary<string, string> MetaData
	{
		get => M;
		set => M = value;
	}

	public void Deconstruct( out DateTimeOffset dateTime, out string name, out STATUS status, out byte[] bytes, out Dictionary<string, string> metaData )
	{
		dateTime = DateTime;
		name     = Name;
		status   = Status;
		bytes    = Bytes;
		metaData = MetaData;
	}
}

public sealed class Images : List<Image>;

public sealed class GetImages
{
	// Default print size 5x7 300 dpi
	public const int DEFAULT_HEIGHT = 1500,
	                 DEFAULT_WIDTH  = 2100;


	public IMAGE_TYPE T { get; set; } = IMAGE_TYPE.TRIP;

	[JsonIgnore]
	public IMAGE_TYPE ImageType
	{
		get => T;
		set => T = value;
	}


	public string I { get; set; } = "";

	[JsonIgnore]
	public string ImageNameOrId
	{
		get => I;
		set => I = value;
	}


	public ImageResolution R { get; set; } = new();

	[JsonIgnore]
	public ImageResolution Resolution
	{
		get => R;
		set => R = value;
	}

	public enum IMAGE_TYPE
	{
		TRIP
	}

	public sealed class ImageResolution
	{
		public int H { get; set; } = DEFAULT_HEIGHT;

		[JsonIgnore]
		public int Height
		{
			get => H;
			set => H = value;
		}


		public int W { get; set; } = DEFAULT_WIDTH;

		[JsonIgnore]
		public int Width
		{
			get => W;
			set => W = value;
		}
	}
}