﻿namespace Protocol.Data;

public sealed class ReportReply
{
	public Report.REPORT_FLAGS S { get; set; } = Report.REPORT_FLAGS.NONE;

	[JsonIgnore]
	public Report.REPORT_FLAGS ReportFlags
	{
		get => S;
		set => S = value;
	}

	public string H { get; set; } = "";

	[JsonIgnore]
	public string HtmlReport
	{
		get => H;
		set => H = value;
	}

	public string C { get; set; } = "";

	[JsonIgnore]
	public string Csv
	{
		get => C;
		set => C = value;
	}

	public long T { get; set; }

	[JsonIgnore]
	public long ProcessingToken
	{
		get => T;
		set => T = value;
	}

	public byte[] P { get; set; } = Array.Empty<byte>();

	[JsonIgnore]
	public byte[] Pdf
	{
		get => P;
		set => P = value;
	}
}

public sealed class Report
{
	public string N { get; set; } = "";

	[JsonIgnore]
	public string Name
	{
		get => N;
		set => N = value;
	}

	public DateTimeOffset Rd { get; set; } = DateTimeOffset.Now;

	[JsonIgnore]
	public DateTimeOffset ReportDateTime
	{
		get => Rd;
		set => Rd = value;
	}

	public PAPER_SIZE P { get; set; } = PAPER_SIZE.A4;

	[JsonIgnore]
	public PAPER_SIZE PaperSize
	{
		get => P;
		set => P = value;
	}

	public bool I { get; set; }

	[JsonIgnore]
	public bool IsLandscape
	{
		get => I;
		set => I = value;
	}

	public string S1 { get; set; } = "";

	[JsonIgnore]
	public string StringArg1
	{
		get => S1;
		set => S1 = value;
	}

	public string S2 { get; set; } = "";

	[JsonIgnore]
	public string StringArg2
	{
		get => S2;
		set => S2 = value;
	}

	public string S3 { get; set; } = "";

	[JsonIgnore]
	public string StringArg3
	{
		get => S3;
		set => S3 = value;
	}

	public string S4 { get; set; } = "";

	[JsonIgnore]
	public string StringArg4
	{
		get => S4;
		set => S4 = value;
	}

	public decimal D1 { get; set; }

	[JsonIgnore]
	public decimal DecimalArg1
	{
		get => D1;
		set => D1 = value;
	}

	public decimal D2 { get; set; }

	[JsonIgnore]
	public decimal DecimalArg2
	{
		get => D2;
		set => D2 = value;
	}

	public decimal D3 { get; set; }

	[JsonIgnore]
	public decimal DecimalArg3
	{
		get => D3;
		set => D3 = value;
	}

	public decimal D4 { get; set; }

	[JsonIgnore]
	public decimal DecimalArg4
	{
		get => D4;
		set => D4 = value;
	}

	public DateTimeOffset Dt1 { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTimeArg1
	{
		get => Dt1;
		set => Dt1 = value;
	}

	public DateTimeOffset Dt2 { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTimeArg2
	{
		get => Dt2;
		set => Dt2 = value;
	}

	public DateTimeOffset Dt3 { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTimeArg3
	{
		get => Dt3;
		set => Dt3 = value;
	}

	public DateTimeOffset Dt4 { get; set; } = DateTimeOffset.MinValue;

	[JsonIgnore]
	public DateTimeOffset DateTimeArg4
	{
		get => Dt4;
		set => Dt4 = value;
	}

	public enum PAPER_SIZE
	{
		A4,
		LETTER
	}

#region List Args
	public IList<string> Sl { get; set; } = new List<string>();

	[JsonIgnore]
	public IList<string> StringListArg
	{
		get => Sl;
		set => Sl = value;
	}
#endregion

#region Pdf
	public byte[] Pd { get; set; } = Array.Empty<byte>();

	[JsonIgnore]
	public byte[] Pdf
	{
		get => Pd;
		set => Pd = value;
	}
#endregion

#region Csv
	public string C { get; set; } = "";

	[JsonIgnore]
	public string Csv
	{
		get => C;
		set => C = value;
	}
#endregion

#region Send Flags
	[Flags]
	public enum REPORT_FLAGS : ushort
	{
		NONE,
		NOTHING_FOUND = NONE, // No results found in report
		PRINT         = 1,
		EMAIL         = 1 << 1,
		STORAGE       = 1 << 2,
		PDF           = 1 << 3,
		CSV           = 1 << 4,
		HTML_IN_EMAIL = 1 << 5,

		PRINT_PDF       = PRINT | PDF,
		PRINT_PDF_CSV   = PRINT_PDF | CSV,
		PRINT_CSV       = PRINT | CSV,
		EMAIL_PRINT     = EMAIL | PRINT,
		EMAIL_PDF       = EMAIL | PDF,
		PDF_HTML        = PDF | HTML_IN_EMAIL,
		EMAIL_PDF_HTML  = EMAIL | PDF_HTML,
		EMAIL_CSV       = EMAIL | CSV,
		EMAIL_PRINT_PDF = EMAIL | PDF | PRINT
	}

	public REPORT_FLAGS S { get; set; } = REPORT_FLAGS.PRINT;

	[JsonIgnore]
	public REPORT_FLAGS ReportFlags
	{
		get => S;
		set => S = value;
	}

#region Email
	public List<string> E { get; set; } = [];

	[JsonIgnore]
	public List<string> EmailAddresses
	{
		get => E;
		set => E = value;
	}

	public string Sb { get; set; } = "";

	[JsonIgnore]
	public string EmailSubject
	{
		get => Sb;
		set => Sb = value;
	}

	public string T { get; set; } = "";

	[JsonIgnore]
	public string PlainText
	{
		get => T;
		set => T = value;
	}
#endregion

	public string Sc { get; set; } = "";

	[JsonIgnore]
	public string StorageCategory
	{
		get => Sc;
		set => Sc = value;
	}

	public string Su { get; set; } = "";

	[JsonIgnore]
	public string StorageSubCategory
	{
		get => Su;
		set => Su = value;
	}

	public string Su1 { get; set; } = "";

	[JsonIgnore]
	public string StorageSubCategory1
	{
		get => Su1;
		set => Su1 = value;
	}

	public string Sn { get; set; } = "";

	[JsonIgnore]
	public string StorageName
	{
		get => Sn;
		set => Sn = value;
	}
#endregion
}