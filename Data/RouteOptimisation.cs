﻿namespace Protocol.Data;

public class RouteOptimisationAddresses
{
	public int O { get; set; }

	[JsonIgnore]
	public int TimeZoneOffsetInMinutes
	{
		get => O;
		set => O = value;
	}


	public Companies C { get; set; } = [];

	[JsonIgnore]
	public Companies Companies
	{
		get => C;
		set => C = value;
	}

	public RouteBase.TRAVEL_MODE T { get; set; } = RouteBase.TRAVEL_MODE.SHORTEST;

	[JsonIgnore]
	public RouteBase.TRAVEL_MODE TravelMode
	{
		get => T;
		set => T = value;
	}

	public RouteOptimisationAddresses()
	{
	}

	public RouteOptimisationAddresses( AddressValidationList address )
	{
		C.AddRange( address.Select( a => new Company
		                                 {
			                                 CompanyName  = a.CompanyName,
			                                 Suite        = a.Suite,
			                                 AddressLine1 = a.AddressLine1,
			                                 AddressLine2 = a.AddressLine2,
			                                 City         = a.City,
			                                 Region       = a.Region,
			                                 RegionCode   = a.RegionCode,
			                                 PostalCode   = a.PostalCode,
			                                 Country      = a.Country,
			                                 CountryCode  = a.CountryCode,
			                                 Latitude     = a.Latitude,
			                                 Longitude    = a.Longitude
		                                 } ) );
	}

	public RouteOptimisationAddresses( AddressValidList address )
	{
		C.AddRange( from A in address
		            where A.IsValid
		            select new Company
		                   {
			                   CompanyName  = A.CompanyName,
			                   Suite        = A.Suite,
			                   AddressLine1 = A.AddressLine1,
			                   AddressLine2 = A.AddressLine2,
			                   City         = A.City,
			                   Region       = A.Region,
			                   RegionCode   = A.RegionCode,
			                   PostalCode   = A.PostalCode,
			                   Country      = A.Country,
			                   CountryCode  = A.CountryCode,
			                   Latitude     = A.Latitude,
			                   Longitude    = A.Longitude
		                   }
		          );
	}
}

public class RouteManifest
{
	public Maps.Route.Route R { get; set; } = [];

	[JsonIgnore]
	public Maps.Route.Route Route
	{
		get => R;
		set => R = value;
	}
}

public class RouteManifestRequest
{
	public string M { get; set; } = "";

	[JsonIgnore]
	public string ManifestName
	{
		get => M;
		set => M = value;
	}


	public int L { get; set; }

	[JsonIgnore]
	public int LegNumber
	{
		get => L;
		set => L = value;
	}
}