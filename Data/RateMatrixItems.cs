﻿namespace Protocol.Data;

public class RateMatrixItem
{
	public int Fr { get; set; }

	[JsonIgnore]
	public int FromZoneId
	{
		get => Fr;
		set => Fr = value;
	}

	public int To { get; set; }

	[JsonIgnore]
	public int ToZoneId
	{
		get => To;
		set => To = value;
	}

	public int S { get; set; }

	[JsonIgnore]
	public int ServiceLevelId
	{
		get => S;
		set => S = value;
	}

	public int P { get; set; }

	[JsonIgnore]
	public int PackageTypeId
	{
		get => P;
		set => P = value;
	}

	public int Ve { get; set; }

	[JsonIgnore]
	public int VehicleTypeId
	{
		get => Ve;
		set => Ve = value;
	}

	public decimal V { get; set; }

	[JsonIgnore]
	public decimal Value
	{
		get => V;
		set => V = value;
	}

	public string Fo { get; set; } = "";

	[JsonIgnore]
	public string Formula
	{
		get => Fo;
		set => Fo = value;
	}
}

public class TripRateMatrixItem
{
	// TODO 
}

public class RateMatrixItems : List<RateMatrixItem>;