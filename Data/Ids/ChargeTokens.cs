﻿namespace Protocol.Data;

public class GetChargeTokens
{
	public bool I { get; set; }

	[JsonIgnore]
	public bool IncludeExpired
	{
		get => I;
		set => I = value;
	}
}

public class ChargeToken
{
	public int I { get; set; }

	[JsonIgnore]
	public int Id
	{
		get => I;
		set => I = value;
	}


	public int V { get; set; }

	[JsonIgnore]
	public int AvailableTokens
	{
		get => V;
		set => V = value;
	}


	public decimal S { get; set; }

	[JsonIgnore]
	public decimal SingleTokenValue
	{
		get => S;
		set => S = value;
	}


	public DateTimeOffset E { get; set; } = DateTimeOffset.MaxValue;

	[JsonIgnore]
	public DateTimeOffset Expires
	{
		get => E;
		set => E = value;
	}
}

public class CarrierChargeToken
{
	public string C { get; set; } = "";

	[JsonIgnore]
	public string CarrierId
	{
		get => C;
		set => C = value;
	}


	public decimal R { get; set; }

	[JsonIgnore]
	public decimal SingleTokenRate
	{
		get => R;
		set => R = value;
	}


	public int E { get; set; }

	[JsonIgnore]
	public int ExpiresInDays
	{
		get => E;
		set => E = value;
	}


	public List<ChargeToken> A { get; set; } = [];

	[JsonIgnore]
	public List<ChargeToken> ActiveTokens
	{
		get => A;
		set => A = value;
	}
}

public class ChargeTokens
{
	public decimal D { get; set; }

	[JsonIgnore]
	public decimal DefaultTokenValue
	{
		get => D;
		set => D = value;
	}


	public int E { get; set; }

	[JsonIgnore]
	public int DefaultTokenExpiresInDays
	{
		get => E;
		set => E = value;
	}


	public List<CarrierChargeToken> C { get; set; } = [];

	[JsonIgnore]
	public List<CarrierChargeToken> CarrierChargeTokens
	{
		get => C;
		set => C = value;
	}
}