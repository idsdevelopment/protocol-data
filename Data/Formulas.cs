﻿namespace Protocol.Data;

public class FormulaVariables : List<string>
{
	public new FormulaVariables AddRange( IEnumerable<string> values )
	{
		base.AddRange( values );
		return this;
	}
}

public class EvaluateFormula
{
	public Dictionary<string, string> V { get; set; } = new();

	[JsonIgnore]
	public Dictionary<string, string> Variables
	{
		get => V;
		set => V = value;
	}


	public string E { get; set; } = "";

	[JsonIgnore]
	public string Expression
	{
		get => E;
		set => E = value;
	}


	public string D { get; set; } = "";

	[JsonIgnore]
	public string DataType
	{
		get => D;
		set => D = value;
	}

	public string GetVariableAsString( string name )
	{
		if( !V.TryGetValue( name, out var Value ) )
			Value = "";
		return Value;
	}

	public decimal GetVariableAsDecimal( string name )
	{
		if( !decimal.TryParse( GetVariableAsString( name ), out var Value ) )
			Value = 0;
		return Value;
	}
}

public class FormulaResult
{
	public decimal V { get; set; }

	[JsonIgnore]
	public decimal Value
	{
		get => V;
		set => V = value;
	}


	public List<string> E { get; set; } = [];

	[JsonIgnore]
	public List<string> ErrorList
	{
		get => E;
		set => E = value;
	}
}